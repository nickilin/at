package food;

public class Grass extends Food {
    private static final String FOOD_NAME = "grass";

    @Override
    public String food() {
        return FOOD_NAME;
    }
}

