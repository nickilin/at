package food;

public class Meat extends Food {
    public static final String FOOD_NAME = "meat";

    @Override
    public String food() {
        return FOOD_NAME;
    }
}
