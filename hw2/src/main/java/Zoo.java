import animals.*;
import food.Grass;
import food.Meat;
import worker.Worker;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker(); //Определяем объекты классов
        Grass grass = new Grass();
        Meat meat = new Meat();
        Cow cow = new Cow();
        cow.setAnimalName("Cow");
        cow.run();
        Lion lion = new Lion();
        lion.setAnimalName("Lion");
        lion.run();
        Duck duck = new Duck();
        duck.setAnimalName("Duck");
        duck.fly();
        Fish fish = new Fish();
        fish.setAnimalName("Fish");
        fish.swim();
        worker.feed(cow, grass);  //Вызываем работника
        worker.getVoice(cow);
        worker.feed(fish, grass); //Рыба хищная не есть траву
        worker.feed(fish, meat); //Рыба хищная есть мясо
        //worker.getVoice(fish);    //Рыба не разговаривает и программа не компилируется
        worker.feed(lion, meat);
        worker.getVoice(lion);
        Swim[] animalsInPond = new Swim[]{new Fish(), new Duck(), new Fish(), new Duck()};  //Создаем пруд с 4 животными
        swimmingAnimals(animalsInPond);
    }

    private static void swimmingAnimals(Swim[] swim) {
        for (Swim s : swim) s.swim();
    }
}
