package animals;

public class Woolf extends Carnivorous implements Run, Voice {
    String voice = "puff";

    @Override
    public void run() {
        System.out.println("Woolf is running");
    }

    @Override
    public String voice() {
        return "Woolf said " + voice;
    }
}
