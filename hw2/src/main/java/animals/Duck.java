package animals;

public class Duck extends Herbivore implements Swim, Fly, Voice {
    String voice = "quack";

    @Override
    public void swim() {
        System.out.println("Duck is swimming");
    }
    /*public void swim() {
        System.out.println("Duck is swimming");
    }*/

    @Override
    public void fly() {
        System.out.println("Duck is flying");
    }

    @Override
    public String voice() {
        return "Duck said " + voice;
    }
}
