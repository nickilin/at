package animals;

public class Lion extends Carnivorous implements Run, Voice {
    String voice = "rrrr";

    @Override
    public void run() {
        System.out.println("Lion is running");
    }

    @Override
    public String voice() {
        return "Lion said " + voice;
    }
}
