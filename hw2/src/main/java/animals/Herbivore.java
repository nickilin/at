package animals;

import food.Food;
import food.Grass;
import food.Meat;

public abstract class Herbivore extends Animal {
    @Override
    public String eat(Food food) {
        return food instanceof Meat ? "doesnt eat meat" : "eats grass";
    }
}
