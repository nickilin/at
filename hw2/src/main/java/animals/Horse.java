package animals;

public class Horse extends Herbivore implements Run, Voice {
    String voice = "neigh";

    @Override
    public void run() {
        System.out.println("Horse is running");
    }

    @Override
    public String voice() {
        return "Horse said " + voice;
    }
}
