package animals;

public class Cow extends Herbivore implements Run, Voice {
    String voice = "moo";

    @Override
    public void run() {
        System.out.println("Cow is running");
    }

    @Override
    public String voice() {
        return "Cow said " + voice;
    }
}
