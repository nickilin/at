package animals;

import food.Food;

public abstract class Animal {
    private String animalName;

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public abstract String eat(Food food);
}