package animals;

import food.Food;
import food.Grass;

public abstract class Carnivorous extends Animal {
    @Override
    public String eat(Food food) {
        return food instanceof Grass ? "doesnt eat grass" : "eats meat";
    }
}
