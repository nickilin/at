package model;

public class Kotik {
    private static int fullness = 5; //начальная сытость
    private static int numberOfClasses;
    private int prettiness;
    private int weight;
    private String name;
    private String meow;

    public int getNumberOfClasses() {
        return numberOfClasses;
    }

    public Kotik() {
        numberOfClasses++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        numberOfClasses++;
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public String getMeow() {
        return meow;
    }

    private boolean eat(int nutritionValue) {
        fullness = fullness + nutritionValue; //увеличиваем сытость едой
        System.out.println("Котик ест [" + fullness + "]");
        return checkWellfed();
    }

    private boolean eat(int nutritionValue, String foodName) {
        fullness = fullness + nutritionValue;
        System.out.println("Котик ест " + foodName + " [" + fullness + "]");
        return checkWellfed();
    }

    private boolean eat() {
        return eat(3, "Wiskas");
    }

    private boolean play() {
        boolean wellfed = checkWellfed();
        if (wellfed) {
            System.out.println("Котик играет [" + fullness + "]");
            fullness--;  //уменьшаем сытость после каждого действия
        }
        return wellfed;
    }

    private boolean sleep() {
        boolean wellfed = checkWellfed();
        if (wellfed) {
            System.out.println("Котик спит [" + fullness + "]");
            fullness--;
        }
        return wellfed;
    }

    private boolean purr() {
        boolean wellfed = checkWellfed();
        if (wellfed) {
            System.out.println("Котик мурлычет [" + fullness + "]");
            fullness--;
        }
        return wellfed;
    }

    private boolean hunt() {
        boolean wellfed = checkWellfed();
        if (wellfed) {
            System.out.println("Котик охотится [" + fullness + "]");
            fullness--;
        }
        return wellfed;
    }

    private boolean checkWellfed() {         //проверяем на сытость
        return fullness > 0;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            if (fullness > 0) {
                int rnd = (int) (Math.random() * 5 + 1);
                switch (rnd) {
                    case (1):
                        eat();
                        break;
                    case (2):
                        play();
                        break;
                    case (3):
                        sleep();
                        break;
                    case (4):
                        purr();
                        break;
                    case (5):
                        hunt();
                        break;
                }
            } else {
                eat();
            }
        }
    }
}
