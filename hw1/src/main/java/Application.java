import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kot1 = new Kotik(10, "Ice", 8, "miu");
        Kotik kot2 = new Kotik();
        kot2.setKotik(8, "Black", 15, "myau");
        System.out.println("Вашего котика зовут - " + kot2.getName() + ". Его вес - " + kot2.getWeight() + ".");
        String howMeow;
        if (kot2.getMeow().equalsIgnoreCase(kot1.getMeow())) {
            howMeow = "одинаково";
        } else {
            howMeow = "по-разному";
        }
        System.out.println("У него есть друг, они мяукают " + howMeow + " - " + kot2.getMeow() + " и " + kot1.getMeow() + ".");
        kot2.liveAnotherDay();
        System.out.println("Число экземпляров класса:" + kot1.getNumberOfClasses());
    }
}
